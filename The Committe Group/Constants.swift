//
//  Constants.swift
//  The Committe Group
//
//  Created by Sanchan on 17/02/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

let kMainUrl = "http://34.198.185.124/"
let kActivationCodeUrl = kMainUrl + "getActivationCode"
let kCarouselUrl = "http://34.198.185.124/getCarousels?device=AppleTV"
let kAccountInfoUrl = "http://34.198.185.124/getAccountInfo"
let kCarouselDataUrl = "http://34.198.185.124/getCarouselData/"
let kAssestDataUrl = "http://34.198.185.124/getAssestData"
let kRecentlyWatchedUrl = "http://34.198.185.124/createRecentlyWatched"
let kManageListUrl = "http://34.198.185.124/manageMyList"
let kUpdateseekUrl = "http://34.198.185.124/updateSeekTime"
let kFetchMyListUrl = "http://34.198.185.124/fetchMyList"
let kFetchRecentUrl = "http://34.198.185.124/fetchRecentlyWatched"
let kLogUrl = "http://34.198.185.124/insertLog"

let kUserType = "device"
let kMovieart = "movie_art"
let kMetadata = "metadata"
let kCarouselId = "carousel_id"
let kData = "data"
let kBoardname = "Main"
let kIsmenu = Bool()
var gUserID = String()
var accountresponse = Bool()
extension UICollectionViewCell
{
    func becomeFocusedUsingAnimationCoordinator(coordinator: UIFocusAnimationCoordinator) {
        coordinator.addCoordinatedAnimations({ () -> Void in
            self.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 10, height: 10)
            self.layer.shadowOpacity = 0.2
            self.layer.shadowRadius = 5
            self.layer.cornerRadius = 10
        }) { () -> Void in
        }
    }
    func addParallaxMotionEffects(tiltValue : CGFloat = 0.1, panValue: CGFloat = 1) {
        var xTilt = UIInterpolatingMotionEffect()
        var yTilt = UIInterpolatingMotionEffect()
        var xPan = UIInterpolatingMotionEffect()
        var yPan = UIInterpolatingMotionEffect()
        let motionGroup = UIMotionEffectGroup()
        xTilt = UIInterpolatingMotionEffect(keyPath:
            "layer.transform.rotation.y", type: .tiltAlongHorizontalAxis)
        xTilt.minimumRelativeValue = -tiltValue
        xTilt.maximumRelativeValue = tiltValue
        yTilt = UIInterpolatingMotionEffect(keyPath:
            "layer.transform.rotation.x", type: .tiltAlongVerticalAxis)
        yTilt.minimumRelativeValue = -tiltValue
        yTilt.maximumRelativeValue = tiltValue
        xPan = UIInterpolatingMotionEffect(keyPath: "center.x", type:     .tiltAlongHorizontalAxis)
        xPan.minimumRelativeValue = -panValue
        xPan.maximumRelativeValue = panValue
        yPan = UIInterpolatingMotionEffect(keyPath: "center.y", type:    .tiltAlongVerticalAxis)
        yPan.minimumRelativeValue = -panValue
        yPan.maximumRelativeValue = panValue
        motionGroup.motionEffects = [xTilt, yTilt, xPan, yPan]
        self.addMotionEffect( motionGroup )
    }
    
}

extension UIImageView
{
    func setgradient()
    {
        let gradient = CAGradientLayer()
        let gradient1 = CAGradientLayer()
        gradient.frame = self.bounds
        gradient1.frame = self.bounds
        
        let startColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        // let endColor = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 1).cgColor
        let endColor = (UIColor.darkGray.withAlphaComponent(0.2)).cgColor
        let startColor1 = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 0.1).cgColor
        let endColor1 = UIColor(colorLiteralRed: 0, green: 0, blue: 0, alpha: 1).cgColor
        gradient1.colors = [startColor1,endColor1]
        gradient.colors = [startColor, endColor]
        gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.isOpaque = false
        gradient1.isOpaque = false
        //  gradient.locations = [0.0,  0.3, 0.5, 0.7, 1.0]
        self.layer.insertSublayer(gradient, at: 0)
        self.layer.insertSublayer(gradient1, at: 0)
    }
}
extension String {
    func widthWithConstrainedWidth(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.width
    }
}

func stringFromTimeInterval(interval:TimeInterval) -> String {
    let ms = NSInteger(interval)
    var seconds = ms/1000
    var minutes = seconds/60
    seconds = seconds%60
    let hours = minutes/60
    minutes = minutes%60
    if hours == 0
    {
        return String("\(minutes)m")
    }
    else
    {
        return String("\(hours)h" + " " + "\(minutes)m")
    }
}
func isConnectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
    if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
        return false
    }
    let isReachable = flags == .reachable
    let needsConnection = flags == .connectionRequired
    return isReachable && !needsConnection
}


